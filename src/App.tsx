import React from "react";
import Root from "./pages/Root";
import { BrowserRouter } from "react-router-dom";

import { Provider } from "react-redux";
import { store } from "./stores";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Root />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
