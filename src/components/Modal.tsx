import React from "react";
import styled from "styled-components";

import { useTransition, animated } from "react-spring";
import { storage } from "../helpers/storage";

type ModalType = {
  onCancel: () => void;
  isShow: boolean;
  showDataInModal: any;
};

type ShotProp = {
  images: ImagesProp;
};

type ImagesProp = {
  normal: string;
  hidpi: string;
};

const Modal = ({ isShow, onCancel, showDataInModal }: ModalType) => {
  const userData = storage.get();

  const props = useTransition(isShow, null, {
    from: { opacity: 0, marginTop: "-50px" },
    enter: { opacity: 1, marginTop: "0px" },
    leave: { opacity: 0, marginTop: "150px" },
  });

  const propsWrapper = useTransition(isShow, null, {
    from: { opacity: 0 },
    enter: { opacity: 0.4 },
    leave: { opacity: 0 },
  });

  return (
    <React.Fragment>
      {propsWrapper.map(({ item, styles, key }: any) => {
        return item ? (
          <ModalWrapper key={key} style={styles}>
            {props.map(({ item, props, key }: any) => {
              return item ? (
                <animated.div
                  key={key}
                  style={props}
                  className="modal__content"
                >
                  {/* start children */}
                  <div className="modal-wrapper-children">
                    <header>
                      <div className="user-inf">
                        <img src={userData["avatar_url"]} alt="LOGO_TIPO" />
                        <div className="user-inf-about-image">
                          <span className="image-title">
                            {showDataInModal.title}
                          </span>
                          <span className="author-name">
                            by{" "}
                            <strong style={{ color: "#ea4c89" }}>
                              {userData["name"]}
                            </strong>
                          </span>
                        </div>
                      </div>
                      <img
                        src={require("../assets/images/close.png")}
                        alt=""
                        onClick={onCancel}
                      />
                    </header>
                    <main>
                      <img src={showDataInModal.images.hidpi} alt="" />
                    </main>
                    <footer>
                      {showDataInModal.description ? (
                        <div
                          dangerouslySetInnerHTML={{
                            __html: showDataInModal.description,
                          }}
                        />
                      ) : (
                        <p>
                          Lorem ipsum, dolor sit amet consectetur adipisicing
                          elit. Animi odio iste nihil reiciendis minima,
                          molestiae aliquam deserunt enim ea et voluptas facilis
                          veritatis voluptatem dolores excepturi eaque repellat
                          itaque est.
                        </p>
                      )}
                    </footer>
                  </div>
                  {/* end children */}
                </animated.div>
              ) : null;
            })}
          </ModalWrapper>
        ) : null;
      })}
    </React.Fragment>
  );
};

const ModalWrapper = styled(animated.div)`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.5);

  .modal-wrapper-children {
    width: 90%;
    margin: 0 auto;
    padding: 10px;
    background-color: white;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .modal-wrapper-children main img {
    width: 100%;
  }

  .modal-wrapper-children header {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 14px;
  }

  .modal-wrapper-children header img {
    height: 18px;
    width: 18px;
    cursor: pointer;
  }

  .modal-wrapper-children header .user-inf {
    display: flex;
    align-items: center;
  }

  .modal-wrapper-children header .user-inf img {
    height: 40px;
    width: 40px;
    object-fit: contain;
    background-color: #ffd700;
    border-radius: 100%;
    margin-right: 10px;
  }

  .user-inf-about-image span {
    display: block;
  }

  .user-inf-about-image .image-title {
  }

  .user-inf-about-image .author-name {
    font-size: 12px;
  }

  @media (min-width: 768px) {
    .modal-wrapper-children {
      width: 700px;
    }

    .modal-wrapper-children main img {
      height: 519px;
    }
  }
`;

export default Modal;
