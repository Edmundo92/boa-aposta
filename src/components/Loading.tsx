import React from "react";
import styled from "styled-components";

const LoadContainer = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: #ffd700;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    color: #000000;
    font-size: 16px;
    line-height: 18px;
    font-weight: bold;
    font-style: italic;
  }
`;

type LoadingProps = {
  isShow: boolean;
};
export const Loading = ({ isShow }: LoadingProps) => {
  return (
    <LoadContainer>
      <span>ACESSANDO ...</span>
    </LoadContainer>
  );
};
