import axios from "axios";
import { storage } from "../helpers/storage";

const api = axios.create({
  baseURL: "https://api.dribbble.com",
});

try {
  if (storage.get()) {
    api.interceptors.request.use(async (config) => {
      const token = await storage.getToken();
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    });
  }
} catch (err) {
  console.warn(err);
}

export default api;
