import { combineReducers } from "redux";

// reducers
import { shotsReducer } from "../pages/Home/shots-reducer";
import { tokenReducer } from "../pages/Login/token-reducer";

const rootReducer = combineReducers({
  token: tokenReducer,
  shots: shotsReducer,
});

export default rootReducer;
