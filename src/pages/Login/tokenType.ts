export const TokenTypes = {
  GET_TOKEN: "GET_TOKEN",
};

export type TokenProps = {
  scope: string;
  created_at: number;
  token_type: string;
  access_token: string;
  avatar_url?: string;
  name?: string;
};

export type UserProps = {
  avatar_url: string;
  name: string;
};
