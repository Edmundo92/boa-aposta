import axios from "axios";
import api from "../../services/api";
import { UserProps } from "./tokenType";
import { storage } from "../../helpers/storage";
import { TokenTypes, TokenProps } from "./tokenType";

export const getTokeAction = (code: string) => {
  return (dispatch: any) => {
    return axios
      .post(
        `https://dribbble.com/oauth/token?client_id=${process.env.REACT_APP_CLIENTE_ID}&client_secret=${process.env.REACT_APP_SECRET_ID}&code=${code}`
      )
      .then((res) => {
        const data = res.data;
        storage.set(data);
        dispatch(setUser(data));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

function setUser(dataToken: TokenProps) {
  return (dispatch: any) => {
    api
      .get(`/v2/user?access_token=${dataToken.access_token}`)
      .then((res) => {
        const d = addDataInObject(dataToken, res.data);
        storage.set(d);
        dispatch({
          type: TokenTypes.GET_TOKEN,
          payload: { data: d, hasSuccess: true },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

function addDataInObject(oldObj: TokenProps, newObj: UserProps) {
  for (let key in newObj) {
    if (key === "name" || key === "avatar_url") {
      oldObj[key] = newObj[key];
    }
  }

  return oldObj;
}
