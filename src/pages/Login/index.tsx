import React, { useEffect, useState } from "react";
import { Button } from "./styles";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { useLocation, useHistory } from "react-router-dom";

import { Loading } from "../../components/Loading";
import { TokenProps } from "./tokenType";
import { getTokeAction } from "./token-action";
import {
  Image,
  Wrapper,
  Text,
  View,
} from "../../assets/styles/custom-elements";

type LoginProps = {
  getTokeAction: (code: string) => void;
  token: {
    data: TokenProps;
    hasSuccess: boolean;
  };
};

type StateProps = {
  token: {
    data: TokenProps;
    hasSuccess: boolean;
  };
};

const Login = ({ token, getTokeAction }: LoginProps) => {
  const location = useLocation();
  const history = useHistory();
  const [showLoading, setShowLoading] = useState(false);

  function getAuthorization() {
    setShowLoading(true);
    window.location.href = `https://dribbble.com/oauth/authorize?client_id=${process.env.REACT_APP_CLIENTE_ID}&redirect_uri=http://localhost:3000/login-response&scope=public`;
  }

  useEffect(() => {
    if (location.search.length > 0) {
      setShowLoading(true);
      let code = location.search.split("=")[1];
      getTokeAction(code);
    }
  }, [location, getTokeAction]);

  useEffect(() => {
    if (token.hasSuccess) {
      setShowLoading(false);
      history.push("/home");
      token.hasSuccess = false;
    }
  }, [token.hasSuccess, history]);

  return (
    <>
      <Wrapper>
        <View>
          <Image
            src={require("../../assets/images/logo.png")}
            alt=""
            margin="0 auto 46px auto"
          />
          <Button onClick={getAuthorization}>
            <Text textStyle="italic" textAlign="left">
              Entrar
            </Text>
            <Image
              src={require("../../assets/images/arrow_right.png")}
              alt="Arrow Right"
            />
          </Button>
        </View>
      </Wrapper>
      {showLoading && <Loading isShow={showLoading} />}
    </>
  );
};

const mapStateToProps = ({ token }: StateProps) => ({
  token,
});
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ getTokeAction }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
