import styled from "styled-components";

export const Button = styled.button`
  width: 330px;
  height: 54px;
  cursor: pointer;
  background-color: #ffd700;
  box-shadow: 0px 2px 4px rgba(51, 46, 46, 0.15);
  border-radius: 6px;
  border-color: transparent;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px;

  .logo {
    display: block;
    margin: 0 auto 46px auto;
  }
`;
