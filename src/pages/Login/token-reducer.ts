import { TokenTypes, TokenProps } from "./tokenType";

const INITIAL_STATE = {
  data: TokenTypes,
  hasSuccess: false,
};

type ActionProps = {
  type: string;
  payload: TokenProps;
};

export const tokenReducer = (state = INITIAL_STATE, action: ActionProps) => {
  switch (action.type) {
    case TokenTypes.GET_TOKEN:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
