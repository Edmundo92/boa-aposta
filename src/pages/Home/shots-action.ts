import { Dispatch } from "redux";
import api from "../../services/api";
import { ShotTypes } from "./shots-type";
import { storage } from "../../helpers/storage";

export const getShotsAction = () => {
  return (dispatch: Dispatch) => {
    api
      .get(`/v2/user/shots?access_token=${storage.getToken()}`)
      .then((res) => {
        dispatch({ type: ShotTypes.GET_SHOTS, payload: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
