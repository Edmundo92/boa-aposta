export const ShotTypes = {
  GET_SHOTS: "GET_SHOTS",
};

export type StateShotProps = {
  shots: [ShotProps];
};

export type ShotProps = {
  animated: false;
  attachments: [];
  description: string;
  height: number;
  html_url: string;
  id: number;
  images: ShotImageProps;
  low_profile: boolean;
  projects: [];
  published_at: string;
  tags: [];
  title: string;
  updated_at: string;
  video: null;
  width: number;
};

type ShotImageProps = {
  four_x: string;
  hidpi: string;
  normal: string;
  one_x: string;
  two_x: string;
};

export type HomeProps = {
  getShotsAction: () => void;
  shots: [ShotProps];
};
