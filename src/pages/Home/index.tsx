import React, { useEffect, useState } from "react";
import { useSpring } from "react-spring";
import Modal from "../../components/Modal";
import { useHistory } from "react-router-dom";

import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { HomeProps, StateShotProps, ShotProps } from "./shots-type";

// actions
import { getShotsAction } from "./shots-action";
import { storage } from "../../helpers/storage";
import { Container, Row, Card } from "../../assets/styles/grid";
import {
  Image,
  Avatar,
  Text,
  View,
  Header,
  DropDown,
  ContentAvatar,
} from "../../assets/styles/custom-elements";

const Home = ({ shots, getShotsAction }: HomeProps) => {
  const userData = storage.get();
  const history = useHistory();
  const [isShowModal, setIsShowModal] = useState(false);
  const [showDataInModal, setShowDataInModal] = useState({});

  const [isToggled, setToggle] = useState(false);
  const menuAppear = useSpring({
    transform: isToggled ? "translate3D(0,0,0)" : "translate3D(0,-20px,0)",
    opacity: isToggled ? 1 : 0,
  });

  useEffect(() => {
    getShotsAction();
  }, [getShotsAction]);

  useEffect(() => {}, [shots]);

  function showSpecificImage(shot: ShotProps) {
    setIsShowModal(true);
    setShowDataInModal(shot);
  }

  function logout() {
    storage.remove();
    history.push("/login-response");
  }

  return (
    <>
      <View>
        <Container>
          <Header>
            <Image
              src={require("../../assets/images/logo.png")}
              alt="LogoTipo"
              width="138px"
            />
            <View position="relative">
              <ContentAvatar>
                <Text margin="0 10px" fontWeight="200px">
                  {userData["name"]}
                </Text>
                <Avatar
                  src={userData["avatar_url"]}
                  alt="Avatar"
                  onClick={() => setToggle(!isToggled)}
                />
              </ContentAvatar>

              {isToggled && (
                <DropDown style={menuAppear}>
                  <View position="relative">
                    <Image
                      onClick={() => logout()}
                      src="https://freepikpsd.com/wp-content/uploads/2019/10/desligar-png-Transparent-Images.png"
                      alt=""
                    />
                    <Text margin="0 10px">Sair</Text>
                  </View>
                </DropDown>
              )}
            </View>
          </Header>
        </Container>

        <View>
          <Container>
            <Row>
              {shots &&
                shots.map((shot: ShotProps) => (
                  <Card key={shot.id} onClick={() => showSpecificImage(shot)}>
                    <Image src={shot.images.normal} />
                  </Card>
                ))}
            </Row>
          </Container>
        </View>
      </View>

      {isShowModal && (
        <Modal
          showDataInModal={showDataInModal}
          isShow={isShowModal}
          onCancel={() => setIsShowModal(false)}
        />
      )}
    </>
  );
};

const mapStateToProps = ({ shots }: StateShotProps) => ({ shots });
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ getShotsAction }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
