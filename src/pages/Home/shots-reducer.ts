import { ShotTypes, ShotProps } from "./shots-type";

const INITIAL_STATE: any = [];

type ActionProps = {
  type: string;
  payload: [ShotProps];
};

export const shotsReducer = (state = INITIAL_STATE, action: ActionProps) => {
  switch (action.type) {
    case ShotTypes.GET_SHOTS:
      return [...action.payload];
    default:
      return state;
  }
};
