import React from "react";
import { Route, Switch } from "react-router-dom";

import Login from "./Login";
import PrivateRoute from "../routes/PrivateRoute";
import Home from "./Home";

const Root = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Login />
      </Route>
      <Route path="/login-response">
        <Login />
      </Route>
      <PrivateRoute path="/home">
        <Home />
      </PrivateRoute>
    </Switch>
  );
};

export default Root;
