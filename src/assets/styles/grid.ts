import styled from "styled-components";

export const Container = styled.div`
  padding-left: 20px;
  padding-right: 20px;

  @media (min-width: 768px) {
    padding-left: 32px;
    padding-right: 32px;
  }

  @media (min-width: 1200px) {
    padding-left: 72px;
    padding-right: 72px;
  }
`;

export const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(270px, 1fr));
  grid-gap: 13px;
`;

export const Card = styled.div`
  border-radius: 10px;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    cursor: pointer;
  }
`;
