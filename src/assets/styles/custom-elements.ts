import styled from "styled-components";
import { animated } from "react-spring";

export const Wrapper = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.img.attrs((props) => {})`
  display: block;
  width: ${(props) => props.width || "auto"};
  margin: ${(props) => props.margin || "0px"};
`;

export const Avatar = styled.img.attrs((props) => {})`
  height: ${(props) => props.height || "70px"};
  width: ${(props) => props.width || "70px"};
  border-radius: 100%;
  object-fit: contain;
  background-color: #ffd700;
  cursor: pointer;
`;
export const DropDown = styled(animated.div)`
  height: 200px;
  width: 200px;
  z-index: 10;
  background-color: white;
  position: absolute;
  right: 0;
  bottom: -207px;
  border-radius: 4px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    height: 60px;
    cursor: pointer;
  }

  span {
    display: block;
    margin-top: 10px;
  }

  ::before {
    content: "";
    position: absolute;
    border: 10px solid white;
    top: -19px;
    right: 22px;
    border-left-color: transparent;
    border-right-color: transparent;
    border-top-color: transparent;
  }
`;

export const Text = styled.span.attrs((props) => {})`
  font-size: 16px;
  line-height: 18px;
  font-weight: ${(props) => props.fontWeight || "bold"};
  color: #000000;
  font-style: ${(props) => props.textStyle || "normal"};
  text-align: ${(props) => props.textAlign || "center"};
  margin: ${(props) => props.margin || "0"};
`;

export const Header = styled.header.attrs((props) => {})`
  height: 76px;
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;
  margin-top: 20px;
`;

export const ContentAvatar = styled.header.attrs((props) => {})`
  display: flex;
  align-items: center;
`;

export const View = styled.div.attrs((props) => {})`
  position: ${(props) => props.position || "relative"};
`;
