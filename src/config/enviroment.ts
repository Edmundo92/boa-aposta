import dotenv from "dotenv";
import { resolve } from "path";

dotenv.config({ path: resolve(__dirname, "../../.env") });

export default {
  CLIENTE_ID: process.env.CLIENTE_ID,
  SECRET_ID: process.env.SECRET_ID,
};
